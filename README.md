# Purple WiFi JS Tech Test

## Directions for use

Clone the repo, run the following command in your local directory: `npm i && npm start`

To run tests: `npm test`

## Overview

This test comes with a mock JSON object that mimics a list of user-generated reports that we provide through our reporting suite. Your task is to build a lightweight application in React and Redux that takes this data and outputs the result in a format you deem fit for the UI.

Don't focus too much on presentation, we are more concerned with the functionality behind your application and the approach you took when undertaking the task.

By all means feel free to embellish your UI with whatever CSS and HTML you want, but remember the focus for us will be your React and Redux code.

Ensure your code is commited along the way, and at sensible and logical intervals, as you see fit.

We've outlined the tasks to be completed below. Try not to spend longer than a couple of hours on this. Don't panic if you can't complete them all, it's better to produce best-standards code for a few than rush to complete everything.

**All tasks should have accompanying tests.**

### Task 1
- Display the mock JSON data in a list, ensuring all relevant information is viewable

### Task 2
- Sort the list based on the report name, ascending and descending
- Filter the reports based on the report type

### Task 3
- Delete a report from the UI, providing fall backs in case the request fails
- Add more reports to the JSON object and be able to paginate through the list, 10 at a time

### Deliverable

The deliverable should be in the form of a standard website that we are able to run in a browser. How you get there is up to you - although as a guide we use tools such as `npm` to be able to start our servers and view our local versions off the back of that. Whatever approach you take, please provide clear instructions of how to get the deliverable running.